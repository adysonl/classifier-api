import express, { Application } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import { Constants } from './utils/constants';
import routes from './routes';
import mongoose from 'mongoose';
import { CsvService } from './utils/csv.service';

class App {
    public app: Application;

    constructor() {
        this.app = express();
        this.setConfig();
        this.mongoSetup();
    }

    private mongoSetup() {
        mongoose
            .connect(Constants.DATABASE_URL, { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true })
            .then(() => console.log('database connected!'))
            .catch(error => console.log(error));
    }
    private setConfig() {
        // Enables cors
        this.app.use(cors());
        // Allows us to receive requests with data in json format
        this.app.use(bodyParser.json());

        // Allows us to receive requests with data in x-www-form-urlencoded format
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(`${Constants.BASE_URL}`, routes);
        this.app.use('/uploads', express.static('uploads'));
    }
}

export default new App().app;
