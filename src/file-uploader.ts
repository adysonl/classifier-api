import multer from 'multer';

// Multer File upload settings
const DIR = './uploads/';

const storage = multer.diskStorage({
    destination: (request, file, cb) => {
        cb(null, DIR);
    },
    filename: (request, file, cb) => {
        const date = new Date().toISOString().replace(/\:|\.|\-/gi, '');
        const originalName = file.originalname
            .toLowerCase()
            .split(' ')
            .join('-');
        const fileName = `${date}-${originalName}`;
        cb(null, fileName);
    }
});

export const upload = multer({ storage });
