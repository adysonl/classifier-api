import { Request, Response } from 'express';
import { CsvService } from '../utils/csv.service';

export class FileController {
    static upload = async (request: Request, response: Response) => {
        const url = request.protocol + '://' + request.get('host');
        if (!request.file) {
            console.log('file unavaliable');
            return response.send({ sucess: false });
        } else {
            return response.send({ url: true });
        }
    };

    static uploadCsv = async (request: Request, response: Response) => {
        const url = request.protocol + '://' + request.get('host') + '/uploads/' + request.file.filename;
        return CsvService.getInstance().getHeaders(`uploads/${request.file.filename}`).then(result => {
            return response.send({url, headers: result});
        });
    };

    static getHeaders =  async (request: Request, response: Response) => {
        const url = request.body.url;
        return CsvService.getInstance().getHeaders(url).then(result => {
            return response.send({url, headers: result});
        });
    }
}
