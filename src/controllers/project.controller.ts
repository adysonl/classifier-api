import { Request, Response } from 'express';
import { ProjectService } from '../services/project.service';

export class ProjectController {
    static save = async (request: Request, response: Response) => {
        ProjectService.getInstance()
            .save(request.body)
            .then(project => {
                response.json(project);
            });
    };

    static findAll = async (request: Request, response: Response) => {
        ProjectService.getInstance()
            .findAll()
            .then(projects => {
                response.json(projects);
            });
    };

    static findOne = async (request: Request, response: Response) => {
        // tslint:disable-next-line: radix
        const id = request.params.id;
        ProjectService.getInstance()
            .findOne(id)
            .then(projects => {
                response.json(projects);
            });
    };

    static update = async (request: Request, response: Response) => {
        ProjectService.getInstance()
            .update(request.body)
            .then(projects => {
                response.json(projects);
            });
    };

    static remove = async (request: Request, response: Response) => {
        // tslint:disable-next-line: radix
        const id = Number.parseInt(request.params.id);
        ProjectService.getInstance()
            .remove(id)
            .then(projects => {
                response.json(projects);
            });
    };
}
