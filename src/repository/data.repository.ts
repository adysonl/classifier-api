import { RepositoryBase } from './repository.base';
import { IData } from '../models/data.model';
import dataSchema from '../models/schemas/data.schema';

export class DataRepository extends RepositoryBase<IData> {
    constructor() {
        super(dataSchema);
    }
}
