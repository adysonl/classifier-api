import mongoose, { Document } from 'mongoose';

export class RepositoryBase<T extends Document> {
    model: mongoose.Model<Document>;

    constructor(schemaModel: mongoose.Model<Document>) {
        this.model = schemaModel;
    }

    create(entity: T | T[]) {
        return this.model.create(entity);
    }

    findAll() {
        return this.model.find({}).populate('columns').populate('labels');
    }

    findById(id: string) {
        return this.model.findById(id);
    }

    update(id: string, entity: T) {
        return this.model.updateOne({ _id: id }, entity);
    }

    delete(id: string) {
        return this.model.deleteOne({ _id: id });
    }

    async saveBulk(entities: T[]) {
        return this.model.create(entities);
    }
}