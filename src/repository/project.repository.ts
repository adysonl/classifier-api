import { RepositoryBase } from './repository.base';
import { IProject } from '../models/project.model';
import projectSchema from '../models/schemas/project.schema';

export class ProjectRepository extends RepositoryBase<IProject> {
    constructor() {
        super(projectSchema);
    }
}
