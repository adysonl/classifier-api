import csv from 'csv-parser';
import fs from 'fs';

export class CsvService {
    private static instance: CsvService;

    static getInstance(): CsvService {
        if (CsvService.instance === undefined) {
            CsvService.instance = new CsvService();
        }
        return CsvService.instance;
    }

    toObject(filename: string) {
        const stream = fs.createReadStream(filename);
        return new Promise((resolve: any, reject: any) => {
            const results = new Array();
            stream
                .pipe(csv())
                .on('data', data => results.push(data))
                .on('end', () => resolve(results));
        });
    }

    getHeaders(filename: string) {
        const stream = fs.createReadStream(filename);
        return new Promise((resolve: any, reject: any) => {
            stream.pipe(csv()).on('headers', async (headers: string[]) => {
                resolve(headers.map(header => header.toLowerCase()));
            });
        });
    }
}
