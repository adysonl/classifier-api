export class Constants {
    static readonly PORT = 8080;
    static readonly DATABASE_URL = 'mongodb://127.0.0.1:27017/classifier';
    static readonly BASE_URL = '/classifier/api';
    static readonly PROJECT_SERVICE = '/project';
    static readonly FILE_SERVICE = '/file';
}
