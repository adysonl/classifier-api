import app from './app';
import { Constants } from './utils/constants';

// tslint:disable-next-line: no-console
app.listen(Constants.PORT, () => {
    console.log(`Listening on port ${Constants.PORT}`);
});

export default app;
