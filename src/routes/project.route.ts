import { Router } from 'express';
import { ProjectController } from '../controllers/project.controller';

const router = Router();

router.post('/', ProjectController.save);

router.put('/', ProjectController.update);

router.delete('/', ProjectController.remove);

router.get('/', ProjectController.findAll);

router.get('/:id', ProjectController.findOne);

export default router;