import { Router } from 'express';
import { FileController } from '../controllers/file.controller';
import { upload } from '../file-uploader';

const router = Router();

router.post('/upload', upload.single('file'), FileController.upload);
router.post('/upload/csv', upload.single('file'), FileController.uploadCsv);
router.post('/upload/csv/headers', FileController.getHeaders);

export default router;
