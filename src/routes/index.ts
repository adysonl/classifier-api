import { Router } from 'express';
import { Constants } from '../utils/constants';
import projectRouter from './project.route';
import fileRouter from './file.route';

const routes = Router();

routes.use(Constants.PROJECT_SERVICE, projectRouter);
routes.use(Constants.FILE_SERVICE, fileRouter);

export default routes;