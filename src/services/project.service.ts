import { ProjectRepository } from '../repository/project.repository';
import { IProject } from '../models/project.model';

export class ProjectService {
    private static instance: ProjectService;
    private projectRepository: ProjectRepository;

    static getInstance(): ProjectService {
        if (ProjectService.instance === undefined) {
            ProjectService.instance = new ProjectService();
        }

        return ProjectService.instance;
    }

    private constructor() {
        this.projectRepository = new ProjectRepository();
    }

    async save(project: IProject) {
        return this.projectRepository.create(project);
    }

    async update(project: any) {
        return project;
    }

    async remove(id: number) {
        return id;
    }

    async findOne(id: string) {
        return this.projectRepository.findById(id);
    }

    async findAll() {
        return this.projectRepository.findAll();
    }
}
