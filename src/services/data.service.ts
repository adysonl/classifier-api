import { DataRepository } from '../repository/data.repository';
import { IData } from '../models/data.model';

export class DataService {
    private static instance: DataService;
    private dataRepository: DataRepository;

    static getInstance(): DataService {
        if (DataService.instance === undefined) {
            DataService.instance = new DataService();
        }

        return DataService.instance;
    }

    private constructor() {
        this.dataRepository = new DataRepository();
    }

    async save(data: IData) {
        return this.dataRepository.create(data);
    }
    async update(data: any) {
        return data;
    }
    async remove(id: number) {
        return id;
    }
    async findOne(id: string) {
        return this.dataRepository.findById(id);
    }
    async findAll() {
        return this.dataRepository.findAll();
    }
}
