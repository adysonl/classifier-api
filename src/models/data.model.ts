import { Document } from 'mongoose';
import { DataTypeEnum } from '../utils/enums';

export interface IData extends Document {
    values?: any[];
    classification?: number[];
    dataType?: DataTypeEnum;
}
