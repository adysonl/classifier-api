import { Document } from 'mongoose';
import { IUser } from './user.model';
import { IColumn } from './column.model';
import { ILabel } from './label.model';
import { IData } from './data.model';

export interface IProject extends Document {
    title: string;
    description: string;
    file?: File;
    columns?: IColumn[];
    data?: IData[];
    labels?: ILabel[];
    author?: IUser;
}
