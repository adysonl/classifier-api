import mongoose, { Schema } from 'mongoose';
import { IProject } from '../project.model';

class ProjectSchema {
    static get schema() {
        const projectSchema: Schema = new Schema(
            {
                title: { type: String, required: true },
                description: { type: String, required: true },
                columns: [
                    {
                        field: { type: String, required: true },
                        name: { type: String },
                        dataType: { type: Number }
                    }
                ],
                labels: [{ key: { type: String, required: true }, name: { type: String }, value: { type: String } }],
                data: { type: [Schema.Types.ObjectId], ref: 'Data', default: [], required: false },
                url: { type: String },
                author: { type: Schema.Types.ObjectId, ref: 'Users' }
            },
            { versionKey: false }
        );

        return projectSchema;
    }
}

export default mongoose.model<IProject>('Projects', ProjectSchema.schema);
