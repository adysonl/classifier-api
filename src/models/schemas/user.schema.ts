import mongoose, { Schema } from 'mongoose';
import { IData } from '../data.model';

class DataSchema {
    static get schema() {
        const dataSchema: Schema = new Schema(
            {
                nickname: { type: String, required: true },
                password: { type: String, required: true },
                name: { type: String }

            },
            { versionKey: false }
        );

        return dataSchema;
    }
}

export default mongoose.model<IData>('Data', DataSchema.schema);
