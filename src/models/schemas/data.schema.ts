import mongoose, { Schema } from 'mongoose';
import { IData } from '../data.model';

class DataSchema {
    static get schema() {
        const dataSchema: Schema = new Schema(
            {
                values: { type: [Object], required: true },
                classification: { type: [Number] },
                dataType: { type: Number }

            },
            { versionKey: false }
        );

        return dataSchema;
    }
}

export default mongoose.model<IData>('Data', DataSchema.schema);
