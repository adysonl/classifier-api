import { Document } from 'mongoose';
import { DataTypeEnum } from '../utils/enums';

export interface IColumn extends Document {
    field: string;
    name: string;
    dataType?: DataTypeEnum;
}
