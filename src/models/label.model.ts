import { Document } from 'mongoose';
import { DataTypeEnum } from '../utils/enums';

export interface ILabel extends Document {
    key: string;
    label: string;
    color?: string;
    icon?: string;
    value: any;
    dataType?: DataTypeEnum;
}
